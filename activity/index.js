
/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
function userDetailsInput() {
	let fullName = prompt("What is your Name?");
	let yourAge = prompt("How old are you?");
	let address = prompt("Where do you live");
	alert("Thank you for the input!");

	console.log("Hello, " + fullName);
	console.log("You are " + yourAge + " years old");
	console.log("You live in " + address);
}

userDetailsInput();


/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
function topFiveBand() {
	let firstBand = "Parokya ni Edgar";
	let secondBand = "Boyce Avenue";
	let thirdBand = "Kamikazee";
	let fourthBand = "Air Supply";
	let fifthBand = "Taylor Swift";

	console.log(1 + ". " + firstBand);
	console.log(2 + ". " + secondBand);
	console.log(3 + ". " + thirdBand);
	console.log(4 + ". " + fourthBand);
	console.log(5 + ". " + fifthBand);

}

topFiveBand();



/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
function favMovies() {
	let firstMovie = "The 100"
	let firstMovieRating = "90%" 
	let secondMovie = "Vagabond"
	let secondMovieRating = "92%"
	let thirdMovie = "The Expendables"
	let thirdMovieRating = "86%" 
	let fourthMovie = "The Avengers"
	let fourthMovieRating = "86%" 
	let fifthMovie = "RRR"
	let fifthMovieRating = "93%" 

	console.log(1 + ". " + firstMovie);
	console.log("Ratings: " + firstMovieRating);
	console.log(2 + ". " + secondMovie);
	console.log("Ratings: " + secondMovieRating);
	console.log(3 + ". " + thirdMovie);
	console.log("Ratings: " + thirdMovieRating);
	console.log(4 + ". " + fourthMovie);
	console.log("Ratings: " + fourthMovieRating);
	console.log(5 + ". " + fifthMovie);
	console.log("Ratings: " + fifthMovieRating);
}

favMovies();


/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

printUsers();
function printUsers() {
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
}

